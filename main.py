import telegram
import os


def hello_http(request):
    """ HTTP Cloud Function
    Arg: request (flask.Request)
    Res: arg(s) for flask.make_response
    """
    token = os.environ["TELEGRAM_TOKEN"]

    if request.path == token and request.method == 'POST':
        bot = telegram.Bot(token)
        update = telegram.Update.de_json(request.get_json(force=True), bot)
        chat_id = update.message.chat.id
        # Reply with the same message
        bot.sendMessage(chat_id=chat_id, text=update.message.text)

    return ("I'm a teapot", 418)
